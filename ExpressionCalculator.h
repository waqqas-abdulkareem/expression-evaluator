//
//  ExpressionCalculator.h
//  ExpressionParser
//
//  Created by Waqqas Sheikh on 9/20/12.
//  Copyright (c) 2012 Waqqas Sheikh. All rights reserved.
//

#ifndef __ExpressionParser__ExpressionCalculator__
#define __ExpressionParser__ExpressionCalculator__

#include &lt;iostream&gt;
#include &lt;string&gt;
#include &lt;vector&gt;

class ExpressionCalculator
{
    private:
    
    std::string expression;
    std::vector&lt;std::string&gt; postfixNotationVector();
    int precedence(char);
    float applyOperator(char,float,float);
    
    //Auxiliary functions.
    std::string trim(std::string);
    std::string charToString(char);
    bool isNumber(std::string);
    
    public:
    
    ExpressionCalculator();
    ExpressionCalculator(std::string expression);
    
    void operator=(const std::string&);
    friend std::ostream& operator&lt;&lt;(std::ostream&, const ExpressionCalculator&);
    
    void Expression(std::string expression);
    std::string Expression();
    
    std::string PostfixNotation();
    float Evaluate();
    
};

#endif /* defined(__ExpressionParser__ExpressionCalculator__) */