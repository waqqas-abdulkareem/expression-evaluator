//
//  ExpressionCalculator.cpp
//  ExpressionParser
//
//  Created by Waqqas Sheikh on 9/20/12.
//  Copyright (c) 2012 Waqqas Sheikh. All rights reserved.
//

#include "ExpressionCalculator.h"
#include <algorithm>
#include <vector>
#include <sstream>

ExpressionCalculator::ExpressionCalculator()
{
    
}

ExpressionCalculator::ExpressionCalculator(std::string expression)
{
    Expression(expression);
}


void ExpressionCalculator::Expression(std::string expression)
{
    /*
     Postfix() adds comma after each number to seperate them, except the last number.
     In Evaluate(), numbers are pushed to stack when a comma is read from postfix.
     There is no comma after last number so it is not pushed to stack.
     Simple solution is to place entire expression between parantheses.
     */
    
    this->expression = "("+trim(expression)+")";
}

std::string ExpressionCalculator::Expression()
{
    return expression;
}

void ExpressionCalculator::operator=(const std::string& expression)
{
    Expression(expression);
}

std::ostream& operator<<(std::ostream& os, const ExpressionCalculator& calc)
{
    return os<<calc.expression;
}

std::vector<std::string> ExpressionCalculator::postfixNotationVector()
{
    /*
     STEPS
     1. Get Character.
     2. If character is an operator:
     2.1 If the stack is empty, add character to stack.
     2.2 If the stack isn't empty
     If character has higher precedence then top of stack, add it to the top of stack.
     If the character has lower precedence then top of stack, drain the stack first before adding character to the stack.
     3. If character is '(', add it to stack.
     4. If character is ')', drain stack until you get to '(', remove '('.
     5. Else the character is a number, print it to string.
     6. After reading through entire string, if the stack is not empty then drain it.
     
     */
    
    std::vector<std::string> postfix;
    std::string operators = "+-*/()";
    std::string numberBuilder;
    
    /*
     A large generic stack is not required.
     A simple array suffices.
     */
    
    
    char stack[expression.length()];
    int stackTop = -1;
    bool lastCharWasNum = false;
    
    
    //Step 1: Get Character
    for(int i = 0; i < expression.length();i++)
    {
        
        //Step 2: Check if character is operator.
        if(operators.find(expression[i])!=std::string::npos)
        {
            //The character is an operator.
            
            //Insert space to seperate numbers from operators
            if(lastCharWasNum)
            {
                postfix.push_back(numberBuilder);
                numberBuilder.clear();
                lastCharWasNum = false;
            }
            
            //Step 4.
            if(expression[i] == ')')
            {
                while (stack[stackTop] != '(') {
                    postfix.push_back(charToString(stack[stackTop--]));
                    
                    
                    if(stackTop==-1)
                    {
                        std::cerr<<"Error: Could not find matching paranthesis.\n";
                        postfix.clear();
                        return postfix;
                    }
                }
                
                stackTop--;
            }else if(expression[i] == '(')
            {
                //Step 3: If operator is (, pusj to stack.
                stack[++stackTop] = expression[i];
            }else
            {
                /*
                 If operator has lower precedence than top,
                 drain stack before pushing operator to stack.
                 */
                
                
                while(stackTop > -1 && stack[stackTop]!= '(')
                {
                    if(precedence(stack[stackTop]) > precedence(expression[i]))
                    {
                        postfix.push_back(charToString(stack[stackTop--]));
                        //postfix += stack[stackTop--];
                    }else break;
                    
                }
                
                //If stack is empty, push operator to stack,
                stack[++stackTop] = expression[i];
            }
            
        }else
        {
            /*
             Step 5: If the character is not an operator (a number),
             append it to postfix string.
             */
            
            numberBuilder += expression[i];
            lastCharWasNum = true;
        }
        
        /*Print Stack Trace
         std::cout<<"\n";
         std::cout<<"Processing '"<<expression[i]<<"': \n";
         std::cout<<"\tStack: ";
         for(int i=0;i<=stackTop;i++)
         {
         std::cout<<stack[i];
         i==stackTop?
         std::cout<<".":
         std::cout<<",";
         }
         std::cout<<"\n";
         */
    }
    
    /*
     Step 6: If string has been parsed but stack is not empty,
     drain stack.
     */
    
    
    while(stackTop > -1)
        postfix.push_back(charToString(stack[stackTop--]));
    //postfix += stack[stackTop--];
    
    return postfix;
}

std::string ExpressionCalculator::PostfixNotation()
{
    std::string postfixString;
    std::vector<std::string> postfixVector = postfixNotationVector();
    for(int i=0;i<postfixVector.size();i++)
    {
        postfixString += postfixVector[i];
        
        postfixString += i==(postfixVector.size()-1)?".":",";
        
    }
    return postfixString;
}

int ExpressionCalculator::precedence(char oper)
{
    if(oper =='+'||oper =='-')
        return 1;
    else if(oper == '*'|| oper =='/' )
        return 2;
    else
        return 0;
    
}

float ExpressionCalculator::Evaluate()
{
    std::vector<std::string> postfixVector = postfixNotationVector();
    
    if(postfixVector.empty())
        return 0;
    
    std::string operators = "+-*/()";
    
    float stack[postfixVector.size()];
    float result;
    int stackTop = -1;
    
    for(int i=0;i<postfixVector.size();i++)
    {
        
        if(operators.find(postfixVector[i])!=std::string::npos)
        {
            result = applyOperator(postfixVector[i][0], stack[stackTop--], stack[stackTop--]);
            stack[++stackTop] = result;
        }else
        {
            stack[++stackTop] = ::atof(postfixVector[i].c_str());
        }
        
        
        /*
         std::cout<<"\n";
         std::cout<<"Processing '"<<postfixVector[i]<<"' :\n";
         std::cout<<"\tStack: ";
         for(int i=0;i<=stackTop;i++)
         {
         std::cout<<stack[i];
         i==stackTop?
         std::cout<<".":
         std::cout<<",";
         }
         std::cout<<"\n";
         */
        
        
    }
    return stack[stackTop];
}

float ExpressionCalculator::applyOperator(char oper,float firstNum, float secondNum)
{
    switch(oper)
    {
        case '/':
            return secondNum/firstNum;
            break;
            
        case '*':
            return secondNum * firstNum;
            break;
            
        case '+':
            return secondNum + firstNum;
            break;
            
        case '-':
            return secondNum - firstNum;
            break;
            
        default:return -1;
    }
}

std::string ExpressionCalculator::trim(std::string expression)
{
    expression.erase(remove_if(expression.begin(), expression.end(), isspace),expression.end());
    return expression;
}

std::string ExpressionCalculator::charToString(char c)
{
    std::stringstream ss;
    std::string s;
    ss << c;
    ss >> s;
    return s;
}

bool ExpressionCalculator::isNumber(std::string testString)
{
    return testString.find_first_not_of("0123456789.-") == std::string::npos;
}