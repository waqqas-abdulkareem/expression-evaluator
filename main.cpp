//
//  main.cpp
//  ExpressionParser
//
//  Created by Waqqas Sheikh on 9/20/12.
//  Copyright (c) 2012 Waqqas Sheikh. All rights reserved.
//

#include <iostream>
#include <string>
#include "ExpressionCalculator.h"

void help();

int main(int argc, const char * argv[])
{
    /*
     Test Expressions:
     1. 3+4*5/6 ----> 345*6/+
     2. (300 + 23) * (43 - 21)/(84+7) ------> 300 23 + 43 21 - * 84 7 + /
     3. (4+8)*(6-5)/((3-2)*(2+2)) ----> : 4 8 + 6 5 - * 3 2 – 2 2 + * /
     4. 2/3*((9/5*5/3)+(2*3)) -----> 2 3 / 9 5 / 5 * 3 / 2 3 * + * [FAILS]
     */

    
    bool postfix;
    std::string expression;
    ExpressionCalculator calc;
    
    
    
    if(argc < 3)
    {
        std::cout<<"Too few arguments.\n";
        return 0;
    }else if(strncmp(argv[1], "-postfix", 4)==0)
    {
        postfix = true;
        expression = argv[2];
    }else if(strncmp(argv[1], "-help", 4)==0)
    {
        void help();
    }else
    {
        std::cout<<"\nInvalid Arguments.\n";
        std::cout<<"Arguments should be of the form: \n";
        std::cout<<"\n";
        std::cout<<"./ExpressionParser [option] '[mathematical expression]'\n";
        std::cout<<"\n";
        std::cout<<"Options:\n";
        std::cout<<"-postfix\t:\tPrint expression in postfix notation.\n";
        std::cout<<"-help\t:\tInstructions on how to use application.\n";
        std::cout<<"\n";
        std::cout<<"Mathematical Expression:\n";
        std::cout<<"\n";
        std::cout<<"- Must be surrounded by single quotes.\n";
        std::cout<<"- The following operations are valid: + - * /.\n";
        std::cout<<"\n";
        std::cout<<"Example:\n";
        std::cout<<"\n";
        std::cout<<"./ExpressionParser -postfix '2+2'\n";
        std::cout<<"\n";
    }
    
    if(postfix)
        std::cout<<"\nPostfix: "<<calc.PostfixNotation()<<".\n";
    
    std::cout<<"\n"<<calc.Evaluate()<<".\n";
    
    return 0;
}

void help()
{
    std::cout<<"EXPRESSION PARSER v1.0\n";
    std::cout<<"(c) Waqqas Sheikh";
    std::cout<<"\n";
    std::cout<<"How to use this appliction:\n";
    std::cout<<"\n";
    std::cout<<"./ExpressionParser [option] '[mathematical expression]'\n";
    std::cout<<"\n";
    std::cout<<"Options:\n";
    std::cout<<"-postfix\t:\tPrint expression in postfix notation.\n";
    std::cout<<"-help\t:\tInstructions on how to use application.\n";
    std::cout<<"\n";
    std::cout<<"Mathematical Expression:\n";
    std::cout<<"\n";
    std::cout<<"- Must be surrounded by single quotes.\n";
    std::cout<<"- The following operations are valid: + - * /.\n";
    std::cout<<"\n";
    std::cout<<"Example:\n";
    std::cout<<"\n";
    std::cout<<"./ExpressionParser -postfix '2+2'\n";
    std::cout<<"\n";
}
